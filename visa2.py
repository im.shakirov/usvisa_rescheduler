# -*- coding: utf8 -*-

import logging
logging.basicConfig(format='%(asctime)s - %(levelname)s - %(message)s', level=logging.INFO)

from dataclasses import dataclass
import datetime as dt
from datetime import datetime
import json
import os
import random
import time
import traceback

from dotenv import load_dotenv
import requests
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.ui import WebDriverWait as Wait
from webdriver_manager.chrome import ChromeDriverManager

@dataclass
class VisaSchedulerConfiguration:
    account_username: str = ''
    account_password: str = ''
    country_code: str = ''
    schedule_id: str = ''
    facility_id: str = ''
    current_appointment_date: datetime.date = None
    min_possible_date: datetime.date = None
    telegram_bot_token: str = ''
    telegram_chat_id: str = ''

def parse_configuration() -> VisaSchedulerConfiguration:
    load_dotenv()

    return VisaSchedulerConfiguration(
        account_username=os.getenv('ACCOUNT_USERNAME', ''),
        account_password=os.getenv('ACCOUNT_PASSWORD', ''),
        country_code=os.getenv('COUNTRY_CODE', ''),
        schedule_id=os.getenv('SCHEDULE_ID', ''),
        facility_id=os.getenv('FACILITY_ID', ''),
        current_appointment_date=datetime.strptime(os.getenv('CURRENT_APPOINTMENT_DATE'), '%Y-%m-%d').date(),
        min_possible_date=datetime.strptime(os.getenv('MIN_POSSIBLE_DATE'), '%Y-%m-%d').date(),
        telegram_bot_token=os.getenv('TELEGRAM_BOT_TOKEN', ''),
        telegram_chat_id=os.getenv('TELEGRAM_CHAT_ID', ''),
    )

class  TelegramNotifier:

    def __init__(self, telegram_bot_token: str, telegram_chat_id: str):
        self.token = telegram_bot_token
        self.chat_id = telegram_chat_id

    def notify(self, text):
        try:
            response = requests.post(f'https://api.telegram.org/bot{self.token}/sendMessage', {
                'chat_id': self.chat_id, 
                'text': text
            })
        except Exception as e:
            trace = traceback.format_exc()
            logging.error(f'caught exception on sending notification: [{e}]')
            logging.error(trace)
            return

        if response.status_code != 200:
            logging.error(f'caught non-200 status code on sending notification: [{response.status_code}] response: [{response.text}]')
            return

        logging.info(f'notified [{self.chat_id}] with text [{text}]')

class VisaWebsiteNavigator:

    class __needLogin(Exception):
        def __init__(self, previous: Exception = None):
            self.previous = previous


    JS_SCRIPT = ("var req = new XMLHttpRequest();"
             f"req.open('GET', '%s', false);"
             "req.setRequestHeader('Accept', 'application/json, text/javascript, */*; q=0.01');"
             "req.setRequestHeader('X-Requested-With', 'XMLHttpRequest');"
             f"req.setRequestHeader('Cookie', '_yatri_session=%s');"
             "req.send(null);"
             "return req.responseText;")
    
    def __init__(self, driver: webdriver.Chrome, account_username: str, account_password: str, country_code: str, schedule_id: str):
        self.driver = driver
        self.username = account_username
        self.password = account_password
        self.country_code = country_code
        self.schedule_id = schedule_id

    def login(self):
        max_retries = 10
        sleep_between_retries = 5 * 60

        retry = 0
        while retry < max_retries:
            retry += 1
            try:
                self.__login()
                return
            except Exception as e:
                if retry == max_retries:
                    raise e
                
                trace = traceback.format_exc()
                logging.error(f'caught exception on login: [{e}]')
                logging.error(trace)

                logging.info(f'sleeping for [{sleep_between_retries}] seconds before retrying')
                time.sleep(sleep_between_retries)

    def __login(self):
        logging.info('logging in..')

        # open home page
        self.driver.get(self.__url())
        if self.__is_logged_in():
            logging.info('already logged in')
            return

        self.__sleep()

        self.__click(f'//a[@class="down-arrow bounce"]', 'scrolling banner')
        self.__sleep()

        self.__click(f'//*[@id="header"]/nav/div[1]/div[1]/div[2]/div[1]/ul/li[3]/a', 'opening the signin page')
        Wait(self.driver, 60).until(expected_conditions.presence_of_element_located((By.NAME, 'commit')))
        self.__click(f'//a[@class="down-arrow bounce"]', 'scrolling banner')
        self.__sleep()

        self.__input(f'//input[@id="user_email"]', self.username, 'input username')
        self.__sleep(1, 3)
        self.__input(f'//input[@id="user_password"]', self.password, 'input password')
        self.__sleep(1, 3)

        self.__click(f'//div[contains(@class,"icheckbox")]', 'clicking on the checkbox')
        self.__sleep(1, 3)
        self.__click(f'//*[@name="commit"]', 'clicking on the login button')

        Wait(self.driver, 60).until(expected_conditions.presence_of_element_located((By.XPATH, '//a[contains(text(), "Continue")]')))
        
        logging.info('logged in')

    def __is_logged_in(self):
        logging.info('checking if already logged in..')
        
        try:
            Wait(self.driver, 10).until(expected_conditions.presence_of_element_located((By.XPATH, '//a[contains(text(), "Continue") and contains(@class, "button primary small")]')))
            return True
        except:
            return False

    def get_appointment_dates(self, facility_id: str) -> list[datetime.date]:
        try:
            return self.__get_appointment_dates(facility_id)
        except self.__needLogin as e:
            logging.warning('need to login again')
            self.login()
            return self.__get_appointment_dates(facility_id)

    def __get_appointment_dates(self, facility_id: str) -> list[datetime.date]:
        dates_ajax_url = self.__url(f'schedule/{self.schedule_id}/appointment/days/{facility_id}.json?appointments[expedite]=false')
        logging.info(f'getting appointment dates from [{dates_ajax_url}]')

        yatri_cookie = self.driver.get_cookie("_yatri_session")
        if yatri_cookie is None:
            raise self.__needLogin()

        session = yatri_cookie['value']
        script = self.JS_SCRIPT % (dates_ajax_url, session)

        try:
            content = self.driver.execute_script(script)
        except Exception as e:
            logging.error(f'error while getting appointment dates: {e}, trace: {traceback.format_exc()}')
            logging.error('trying to login again')
            raise self.__needLogin()

        logging.info(f'response: {content}')
        try:
            dates = json.loads(content)
        except Exception as e:
            logging.error(f'error while parsing response: {e}, trace: {traceback.format_exc()}')
            logging.error('trying to login again')
            raise self.__needLogin()

        if 'error' in dates:
            logging.error(f'got error in dates response: {dates["error"]}')
            logging.error('trying to login again')
            raise self.__needLogin()

        return list(map(lambda x: datetime.strptime(x['date'], '%Y-%m-%d').date(), dates))

    def __get_appointment_time(self, facility_id: str, date: datetime.date):
        date_string = date.strftime('%Y-%m-%d')
        times_ajax_url = self.__url(f'schedule/{self.schedule_id}/appointment/times/{facility_id}.json?date={date_string}&appointments[expedite]=false')
        logging.info(f'getting appointment times from [{times_ajax_url}]')

        yatri_cookie = self.driver.get_cookie("_yatri_session")
        if yatri_cookie is None:
            raise self.__needLogin()

        session = yatri_cookie['value']
        script = self.JS_SCRIPT % (times_ajax_url, session)
        content = self.driver.execute_script(script)

        logging.info(f'appointment times response: {content}')

        times = json.loads(content)
        time = times.get("available_times")[-1]

        logging.info(f"got appointment {time} for {date}")

        return time

    def reschedule(self, facility_id: str, date: datetime.date):
        available_time = self.__get_appointment_time(facility_id, date)
        if available_time is None:
            raise Exception(f"no available time for {date} at facility [{facility_id}]")

        logging.info(f"rescheduling for {date} at {available_time} at facility [{facility_id}]")

        self.__go_to_appointment_page()
        appointment_url = self.__url(f'schedule/{self.schedule_id}/appointment')

        headers = {
            "User-Agent": self.driver.execute_script("return navigator.userAgent;"),
            "Referer": appointment_url,
            "Cookie": "_yatri_session=" + self.driver.get_cookie("_yatri_session")["value"]
        }

        data = {
            "utf8": self.driver.find_element(by=By.NAME, value='utf8').get_attribute('value'),
            "authenticity_token": self.driver.find_element(by=By.NAME, value='authenticity_token').get_attribute('value'),
            "confirmed_limit_message": self.driver.find_element(by=By.NAME, value='confirmed_limit_message').get_attribute('value'),
            "use_consulate_appointment_capacity": self.driver.find_element(by=By.NAME, value='use_consulate_appointment_capacity').get_attribute('value'),
            "appointments[consulate_appointment][facility_id]": facility_id,
            "appointments[consulate_appointment][date]": date.strftime('%Y-%m-%d'),
            "appointments[consulate_appointment][time]": available_time,
        }

        r = requests.post(appointment_url, headers=headers, data=data)
        if  r.text.find('You have successfully scheduled your visa appointment.') != -1:
            logging.info(f"rescheduled successfully at date {date} at {available_time} at facility [{facility_id}]")
            return True
        else:
            logging.error(f"reschedule failed")
            logging.error(f"response: {r.text}")
            logging.error(f"response: {r}")
            return False

    def __go_to_appointment_page(self):
        appointment_url = self.__url(f'schedule/{self.schedule_id}/appointment')
        logging.info(f'going to appointment page [{appointment_url}]')
        self.driver.get(appointment_url)

        appointment_title_cond = expected_conditions.presence_of_element_located((By.XPATH, '//legend[contains(text(),"Consular Section Appointment")]'))
        continue_button_cond = expected_conditions.presence_of_element_located((By.XPATH, '//input[@class="button primary" and contains(@value, "Continue")]'))

        try:
            elem = Wait(self.driver, 10).until(expected_conditions.any_of(*[appointment_title_cond, continue_button_cond]))
            if elem.tag_name == 'input': # we found continue button, click it
                logging.info(f'found continue button')
                elem.click()
                Wait(self.driver, 10).until(appointment_title_cond)
            
            logging.info('on appointment page')

        except Exception as e:
            logging.error(f'error waiting for elements: {e}')

    def __click(self, xpath: str, action_description: str = None):
        if action_description:
            logging.info(f'click [{action_description}]')
        
        self.driver.find_element(By.XPATH, xpath).click()

    def __input(self, xpath: str, value: str, action_description: str = None):
        if action_description:
            logging.info(f'input [{action_description}]')
        
        self.driver.find_element(By.XPATH, xpath).send_keys(value)

    def __url(self, path: str = ''):
        return f'https://ais.usvisa-info.com/{self.country_code}/niv/{path}'

    def __sleep(self, seconds: float = 0.5, max_seconds: float = None):
        if max_seconds:
            seconds = random.uniform(seconds, max_seconds)
        
        logging.info(f'sleeping for %.1f seconds..', seconds)
        time.sleep(seconds)

class VisaRescheduler():

    # some comments from reddit
    # https://www.reddit.com/r/immigration/comments/13xslw9/comment/jq37cas/
    # https://www.reddit.com/r/UnethicalLifeProTips/comments/10uw5ge/comment/jcitszc/

    hot_time_window = [dt.time(6, 59), dt.time(7, 20)]
    hot_time_sleep = (10, 30)
    time_sleep = (75, 135)
    empty_responses_before_cooldown = 3
    cooldown_time = 5 * 60 * 60  # 5 hrs, according reddit comments
    
    def __init__(self, navigator: VisaWebsiteNavigator, notifier: TelegramNotifier, facility_id: str, current_appointment_date: datetime.date, min_possible_date: datetime.date):
        self.navigator = navigator
        self.notifier = notifier
        self.facility_id = facility_id
        self.current_appointment_date = current_appointment_date
        self.min_possible_date = min_possible_date
        self.total_iterations = 0
        self.empty_responses = 0
        self.empty_responses_in_row = 0
        self.checks_before_cooldown = 0

    def run(self):
        try:
            self.notifier.notify('starting..')
            self.__run()
            self.notifier.notify('exiting..')
            logging.info('done')
        except Exception as e:
            trace = traceback.format_exc()
            self.notifier.notify(f'failed with exception: {e}\ntrace: {trace}\n\ntotal iterations: {self.total_iterations}, empty responses: {self.empty_responses}\nexiting..')
            logging.error(e)
            logging.error(trace)
        except KeyboardInterrupt:
            self.notifier.notify('interrupted by user, exiting..')
            logging.info('done')

    def __run(self):
        while True:
            stop = self.__iteration()
            if stop:
                break
            self.__sleep_between_iterations()

    def __iteration(self) -> bool: # returns whether to stop the script (False - continue)
        self.total_iterations += 1
        logging.info(f'===== starting iteration #{self.total_iterations} (empty_responses: {self.empty_responses}) =====')

        dates = self.navigator.get_appointment_dates(self.facility_id)
        logging.info('fetched dates: %s', list(map(lambda x: x.strftime('%Y-%m-%d'), dates)))
        if len(dates) == 0:
            self.empty_responses += 1
            self.empty_responses_in_row += 1
            logging.warning('no dates found')
            return False
        else:
            self.empty_responses_in_row = 0

        earliest_date = min(dates)
        if earliest_date < self.current_appointment_date:
            self.notifier.notify(f'found early date: {earliest_date}, rescheduling..')
            if earliest_date < self.min_possible_date:
                logging.warning(f'found earliest date {earliest_date} is before min possible date {self.min_possible_date}')
                self.notifier.notify(f'found earliest date {earliest_date} is before min possible date {self.min_possible_date}, skipping..')
                return False

            success = self.navigator.reschedule(self.facility_id, earliest_date)
            if success:
                self.notifier.notify(f'appointment rescheduled successfully')
                return True
            else:
                self.notifier.notify(f'appointment rescheduling failed, check logs')
        else:
            logging.info(f'no early date found, sleeping..')

        return False

    def __sleep_between_iterations(self):
        current_time = datetime.now().time()

        start_time = self.hot_time_window[0]
        end_time = self.hot_time_window[1]
        
        if self.empty_responses_in_row >= self.empty_responses_before_cooldown:
            logging.info(f'{self.empty_responses_in_row} empty responses in a row, cooldown! sleeping for [{self.cooldown_time}]')
            self.notifier.notify(f'{self.empty_responses_in_row} empty responses in a row, cooldown! sleeping for [{self.cooldown_time}] (total iterations: {self.total_iterations}, empty responses: {self.empty_responses}')
            time.sleep(self.cooldown_time)
            self.empty_responses_in_row = 0
            return

        if start_time < current_time < end_time: # hot time
            sleep_time = random.uniform(self.hot_time_sleep[0], self.hot_time_sleep[1])
            logging.info(f'hot time! sleeping for [{sleep_time}]')
            time.sleep(sleep_time)
            return
        
        sleep_time = random.uniform(self.time_sleep[0], self.time_sleep[1])
        if current_time < start_time:
            until_next_iteration = (datetime.combine(dt.date.today(), start_time) - datetime.combine(dt.date.today(), current_time)).total_seconds()
            sleep_time = min(sleep_time, until_next_iteration)
        
        logging.info(f'sleeping for [{sleep_time}]')
        time.sleep(sleep_time)

def main():
    configuration = parse_configuration()

    navigator = VisaWebsiteNavigator(
        driver=webdriver.Chrome(service=ChromeService(ChromeDriverManager().install())),
        account_username=configuration.account_username,
        account_password=configuration.account_password,
        country_code=configuration.country_code,
        schedule_id=configuration.schedule_id
    )
    notifier = TelegramNotifier(configuration.telegram_bot_token, configuration.telegram_chat_id)

    rescheduler = VisaRescheduler(navigator, notifier, configuration.facility_id, configuration.current_appointment_date, configuration.min_possible_date)

    rescheduler.run()

if __name__ == '__main__':
    main()